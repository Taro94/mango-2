using System.Reflection;
using Microsoft.Extensions.Configuration;

namespace Commons;

/// <summary>
/// Extension for replacing parts of the configuration JSON file.
/// </summary>
public static class ConfigurationBuilderHelper
{
    public static IConfigurationBuilder AddJsonStream(this IConfigurationBuilder builder, Stream stream, List<(string,string)> replacements)
    {
        StreamReader reader = new StreamReader(stream);
        var json = reader.ReadToEnd();
        foreach (var replacement in replacements)
        {
            json = json.Replace(replacement.Item1, replacement.Item2);
        }

        stream = GenerateStreamFromString(json);
        builder = builder.AddJsonStream(stream);
        return builder;
    }
    
    private static Stream GenerateStreamFromString(string s)
    {
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }
}