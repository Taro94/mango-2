using ArchPackagesViewer.Interfaces;

namespace ArchPackagesViewer;

/// <summary>
/// Provider for a list of packages considered native by the operating system.
/// </summary>
public class NativePackagesListProvider : INativePackagesListProvider
{
    private readonly string _extraPathToCheckForListFile;

    /// <summary>
    /// Creates an instance of the native packages list provider.
    /// </summary>
    /// <param name="extraPathToCheckForListFile">If given, the provider will try to retrieve the list of native packages from that file in the first place.</param>
    public NativePackagesListProvider(string extraPathToCheckForListFile = null)
    {
        _extraPathToCheckForListFile = extraPathToCheckForListFile;
    }

    /// <summary>
    /// Returns a list of package names considered native by the operating system.
    /// </summary>
    /// <returns>IEnumerable of strings representing package names.</returns>
    public IEnumerable<string> GetNativePackageNames()
    {
        if (_extraPathToCheckForListFile != null && File.Exists(_extraPathToCheckForListFile))
        {
            return File.ReadAllLines(_extraPathToCheckForListFile);
        }

        var manjaroNativePackagesListPath = "/desktopfs-pkgs.txt";
        if (File.Exists(manjaroNativePackagesListPath))
        {
            return File.ReadAllLines(manjaroNativePackagesListPath).Select(x => x.Split(' ')[0]);
        }

        return Array.Empty<string>();
    }
}