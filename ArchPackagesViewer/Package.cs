﻿using System.Collections.Immutable;
using ArchPackagesViewer.Interfaces;

namespace ArchPackagesViewer;

/// <inheritdoc />
public class Package : IPackage
{
    /// <inheritdoc />
    public string Name { get; set; }
    
    /// <inheritdoc />
    public string Version { get; set; }
    
    /// <inheritdoc />
    public bool Native { get; set; }

    /// <inheritdoc />
    public ImmutableHashSet<IPackage> Dependencies { get; set; }
    
    /// <inheritdoc />
    public ImmutableHashSet<IPackage> InstalledOptionalDependencies { get; set; }
    
    /// <inheritdoc />
    public ImmutableHashSet<RelatedPackageInfo> ProvidedPackagesInfos { get; set; } = ImmutableHashSet<RelatedPackageInfo>.Empty;
    
    /// <inheritdoc />
    public ImmutableHashSet<RelatedPackageInfo> ConflictingPackagesInfos { get; set; } = ImmutableHashSet<RelatedPackageInfo>.Empty;
    
    /// <inheritdoc />
    public ImmutableHashSet<RelatedPackageInfo> OptionalDependenciesInfos { get; set; } = ImmutableHashSet<RelatedPackageInfo>.Empty;
    
    /// <inheritdoc />
    public ImmutableHashSet<RelatedPackageInfo> DependenciesInfos { get; set; } = ImmutableHashSet<RelatedPackageInfo>.Empty;

    /// <inheritdoc />
    public HashSet<IPackage> GetAllRequiredNonNativePackages(bool includeOptional = false, bool firstLevelOptionalOnly = true)
    {
        if (Native)
            return new HashSet<IPackage>();
        
        var result = new List<IPackage> { this };
        foreach (var dep in Dependencies)
            result.AddRange(dep.GetAllRequiredNonNativePackages(includeOptional && !firstLevelOptionalOnly, firstLevelOptionalOnly));
        
        if (!includeOptional)
            return result.ToHashSet();
        
        foreach (var optDep in InstalledOptionalDependencies)
            result.AddRange(optDep.GetAllRequiredNonNativePackages(!firstLevelOptionalOnly, false));
        return result.ToHashSet();
    }

    /// <inheritdoc />
    public HashSet<RelatedPackageInfo> GetAllConflicts(bool includeOptional = false, bool firstLevelOptionalOnly = true)
    {
        if (Native)
            return new HashSet<RelatedPackageInfo>();
        
        var result = ConflictingPackagesInfos.ToList();

        foreach (var dep in Dependencies)
            result.AddRange(dep.GetAllConflicts(includeOptional && !firstLevelOptionalOnly, firstLevelOptionalOnly));
        
        if (!includeOptional)
            return result.ToHashSet();
        
        foreach (var optDep in InstalledOptionalDependencies)
            result.AddRange(optDep.GetAllConflicts(!firstLevelOptionalOnly, false));
        return result.ToHashSet();
    }

    /// <inheritdoc />
    public HashSet<RelatedPackageInfo> GetAllProvisions(bool includeOptional = false, bool firstLevelOptionalOnly = true)
    {
        if (Native)
            return new HashSet<RelatedPackageInfo>();
        
        var result = ProvidedPackagesInfos.ToList();

        foreach (var dep in Dependencies)
            result.AddRange(dep.GetAllProvisions(includeOptional && !firstLevelOptionalOnly, firstLevelOptionalOnly));
        
        if (!includeOptional)
            return result.ToHashSet();
        
        foreach (var optDep in InstalledOptionalDependencies)
            result.AddRange(optDep.GetAllProvisions(!firstLevelOptionalOnly, false));
        return result.ToHashSet();
    }
}