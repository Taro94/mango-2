using ArchPackagesViewer.Enums;

namespace ArchPackagesViewer.Interfaces;

/// <summary>
/// Type for comparing versions of packages expressed as strings.
/// </summary>
public interface IVersionComparer
{
    /// <summary>
    /// Checks if a given version string represents a version higher than another.
    /// </summary>
    /// <param name="versionToCheck">Version to check.</param>
    /// <param name="versionToCompareWith">Version to check against.</param>
    /// <returns>ComparisonResult enum.</returns>
    ComparisonResult CompareVersions(string versionToCheck, string versionToCompareWith);

    /// <summary>
    /// Checks if a given version fulfills the given version requirement.
    /// </summary>
    /// <param name="versionToCheck">Version string to check.</param>
    /// <param name="versionRequirement">Version requirement to check against, should start with comparison and be followed by a version string.</param>
    /// <returns>Boolean value denoting if the version meets the specified requirement.</returns>
    bool IsVersionValid(string versionToCheck, string versionRequirement);

    /// <summary>
    /// Checks if a given version range fulfills the given version requirement.
    /// </summary>
    /// <param name="versionRangeToCheck">Version range string to check, should start with a comparison and be followed by a version string.</param>
    /// <param name="versionRequirement">Version requirement to check against, should start with comparison and be followed by a version string.</param>
    /// <returns>Boolean value denoting if the version meets the specified requirement. Always true if versionToCheck is an empty string.</returns>
    public bool IsVersionRangeValid(string versionRangeToCheck, string versionRequirement);
}