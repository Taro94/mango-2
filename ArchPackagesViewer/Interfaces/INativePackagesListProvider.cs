namespace ArchPackagesViewer.Interfaces;

/// <summary>
/// Provider for a list of packages considered native by the operating system.
/// </summary>
public interface INativePackagesListProvider
{
    /// <summary>
    /// Returns a list of package names considered native by the operating system.
    /// </summary>
    /// <returns>IEnumerable of strings representing package names.</returns>
    IEnumerable<string> GetNativePackageNames();
}