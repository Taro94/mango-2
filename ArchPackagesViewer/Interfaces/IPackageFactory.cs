namespace ArchPackagesViewer.Interfaces;

/// <summary>
/// Factory for IPackage objects.
/// </summary>
public interface IPackageFactory
{
    /// <summary>
    /// Creates an IPackage object.
    /// </summary>
    /// <returns>Instance of an IPackage implementation.</returns>
    IPackage CreatePackage();
}