namespace ArchPackagesViewer.Interfaces;

/// <summary>
/// Type for listing installed packages that make it impossible to install a set of other packages.
/// </summary>
public interface IConflictDetector
{
    /// <summary>
    /// Check if the provided set of packages is not installable (due to conflicts) and return a set of installed packages causing conflicts.
    /// </summary>
    /// <param name="packagesAndProvisionsToCheckAgainstInstalled">Packages to check for ability to be installed without conflicts with installed packages. They need to be in format: "name version" and "version" may be an empty string (a space character is still needed).</param>
    /// <param name="conflictsToCheckAgainstInstalled">Conflicts of the packages and provisions to be installed to check against installed packages.</param>
    /// <returns>Collection of installed packages causing conflicts.</returns>
    HashSet<IPackage> GetInstalledConflictingPackages(IEnumerable<string> packagesAndProvisionsToCheckAgainstInstalled, IEnumerable<string> conflictsToCheckAgainstInstalled);
}