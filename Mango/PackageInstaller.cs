using ArchPackagesViewer.Enums;
using ArchPackagesViewer.Interfaces;
using Commons;
using Mango.Interfaces;
using Mango.Options;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace Mango;

/// <inheritdoc />
public class PackageInstaller : IPackageInstaller
{
    private readonly ISystemPackages _systemPackages;
    private readonly IRootAccessVerifier _rootAccessVerifier;
    private readonly IVersionComparer _versionComparer;
    private readonly IConflictDetector _conflictDetector;
    private readonly IConfiguration _configuration;
    private readonly ILogger _logger;

    public PackageInstaller(ISystemPackages systemPackages, IRootAccessVerifier rootAccessVerifier, IVersionComparer versionComparer, IConflictDetector conflictDetector, IConfiguration configuration, ILogger logger = null)
    {
        _systemPackages = systemPackages;
        _rootAccessVerifier = rootAccessVerifier;
        _versionComparer = versionComparer;
        _conflictDetector = conflictDetector;
        _configuration = configuration;
        _logger = logger;
    }
    
    /// <inheritdoc />
    public int InstallPackage(InstallOptions options)
    {
        _logger?.Information($"Package information printing starting for options: package = {options.Package}");
        
        var temporaryDirectoryPath = "/tmp/mango";

        // Ensure file exists
        if (!File.Exists(options.Package))
        {
            var error = $"File {options.Package} does not exist.";
            _logger?.Error(error);
            Console.Error.WriteLine(error);
            return 1;
        }
        
        // Root is required
        if (!_rootAccessVerifier.GetHasRootAccess())
        {
            var error = "Root access is required to install packages. Try running the command with sudo.";
            _logger?.Error(error);
            Console.Error.WriteLine(error);
            return 1;
        }
        
        // Flush temporary directory
        if (Directory.Exists(temporaryDirectoryPath))
            Directory.Delete(temporaryDirectoryPath, true);
        Directory.CreateDirectory(temporaryDirectoryPath);
        
        // Unpack the file
        Console.WriteLine("Installing...");
        $"tar -xf \"{options.Package}\" -C {temporaryDirectoryPath}".Bash();
        var metaContent = File.ReadAllText(Path.Combine(temporaryDirectoryPath, ".MANGO"));
        var metadata = JsonConvert.DeserializeObject<MangoPackageMetadata>(metaContent);
        if (metadata == null)
        {
            var error = $"File {options.Package} does not contain valid metadata.";
            _logger?.Error(error);
            Console.Error.WriteLine(error);
            return 1;
        }

        // Get packages and provisions to check for conflicts (ensure packages that are installed are excluded)
        var packagesAndProvisionsToCheck = new List<string>();
        var packagesAlreadyInstalled = new List<IPackage>();
        var packageFileNamesNotToInstall = new List<string>();
        foreach (var includedPackage in metadata.Packages)
        {
            var split = includedPackage.Split(' ');
            var name = split[0];
            var version = split[1];

            var installed = _systemPackages[name];
            if (installed != null)
            {
                packagesAlreadyInstalled.Add(installed);
                if (_versionComparer.CompareVersions(installed.Version, version) == ComparisonResult.LargerThan)
                    packageFileNamesNotToInstall.Add($"{name}-{version}.pkg.tar.xz");
                continue;
            }

            var providingPackages = _systemPackages.GetInstalledPackagesProvidingPackage(name);
            if (providingPackages.Any(x => x.ProvidedVersion == version))
                continue;

            packagesAndProvisionsToCheck.Add(includedPackage);
        }
        foreach (var provision in metadata.ProvidedPackages)
        {
            var split = provision.Split('=');
            var name = split[0];
            var version = split.Length > 1 ? split[1] : string.Empty;
            
            if (_systemPackages[name] != null)
                continue;
            
            var providingPackages = _systemPackages.GetInstalledPackagesProvidingPackage(name);
            if ((version == string.Empty && !providingPackages.IsEmpty) || providingPackages.Any(x => _versionComparer.IsVersionValid(version, x.ProvidedVersion)))
                continue;
            
            packagesAndProvisionsToCheck.Add($"{name} {version}");
        }
        
        // Get list of conflicts of the mango package, but exclude "false" conflicts
        var conflicts = metadata.Conflicts.ToHashSet();
        foreach (var alreadyInstalled in packagesAlreadyInstalled)
        foreach (var conflict in alreadyInstalled.ConflictingPackagesInfos)
            conflicts.Remove($"{conflict.Name}{conflict.VersionRequirement}");

        // Check for conflicts
        var conflictingPackages = _conflictDetector.GetInstalledConflictingPackages(packagesAndProvisionsToCheck, conflicts);
        if (conflictingPackages.Any())
        {
            var error = "The package can't be installed because there are Arch packages installed in the system that conflict with the contents of the mango package. See the mango package details for the list of problematic installed packages.";
            _logger?.Error(error);
            Console.Error.WriteLine(error);
            return 1;
        }
        
        // Delete from the directory with packages to install those packages that are already installed in this version or newer
        if (!options.Downgrade)
        {
            foreach (var filename in packageFileNamesNotToInstall)
            {
                var path = Path.Combine(temporaryDirectoryPath, filename);
                if (File.Exists(path))
                    File.Delete(path);
            }
        }

        if (Directory.GetFiles(temporaryDirectoryPath).Length > 1)
        {
            $"pacman -Udd --asdeps --noconfirm --needed {Path.Combine(temporaryDirectoryPath, "*")}".Bash();
            $"pacman -D --asexplicit {metadata.PackageName}".Bash();
        }
        else
        {
            var info = "All the packages are already installed in newer versions. Use the -d flag to force install the mango package in order to downgrade the Arch packages installed.";
            _logger?.Information(info);
            Console.WriteLine(info);
            return 0;
        }

        // Clear temporary directory
        if (Directory.Exists(temporaryDirectoryPath))
            Directory.Delete(temporaryDirectoryPath, true);

        return 0;
    }
}