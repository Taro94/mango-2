using ArchPackagesViewer;
using ArchPackagesViewer.Interfaces;
using Commons;
using Mango.Interfaces;
using Mango.Options;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace Mango;

/// <inheritdoc />
public class PackageInfoPrinter : IPackageInfoPrinter
{
    private readonly ISystemPackages _systemPackages;
    private readonly IConfiguration _configuration;
    private readonly IConflictDetector _conflictDetector;
    private readonly IVersionComparer _versionComparer;
    private readonly ILogger _logger;

    public PackageInfoPrinter(ISystemPackages systemPackages, IConfiguration configuration, IConflictDetector conflictDetector, IVersionComparer versionComparer, ILogger logger = null)
    {
        _systemPackages = systemPackages;
        _configuration = configuration;
        _conflictDetector = conflictDetector;
        _versionComparer = versionComparer;
        _logger = logger;
    }

    /// <inheritdoc />
    public int PrintPackageInformation(PackageOptions options)
    {
        _logger?.Information($"Package information printing starting for options: package = {options.Package}");
        
        var temporaryDirectoryPath = _configuration.GetRequiredSection("TemporaryDirectoryPath")?.Value;
        if (temporaryDirectoryPath == null)
        {
            var fatal = "Temporary directory path is null, appsettings.json was not configured properly at build time.";
            _logger?.Fatal(fatal);
            Console.Error.WriteLine("Fatal error, see the log file for details.");
            return 1;
        }
        
        // Ensure file exists
        if (!File.Exists(options.Package))
        {
            var error = $"File {options.Package} does not exist.";
            _logger?.Error(error);
            Console.Error.WriteLine(error);
            return 1;
        }
        
        // Flush temporary directory
        if (Directory.Exists(temporaryDirectoryPath))
            Directory.Delete(temporaryDirectoryPath, true);
        Directory.CreateDirectory(temporaryDirectoryPath);
        
        // Get metadata
        $"tar -xf \"{options.Package}\" -C {temporaryDirectoryPath} .MANGO".Bash();
        var metaContent = File.ReadAllText(Path.Combine(temporaryDirectoryPath, ".MANGO"));
        var metadata = JsonConvert.DeserializeObject<MangoPackageMetadata>(metaContent);
        if (metadata == null)
        {
            var error = $"File {options.Package} does not contain valid metadata.";
            _logger?.Error(error);
            Console.Error.WriteLine(error);
            return 1;
        }
        
        // Print info
        Console.WriteLine($"Package name: {metadata.PackageName}");
        Console.WriteLine($"Mango version: {metadata.MangoVersion}");
        Console.WriteLine($"Created at: {metadata.CreatedAt}");
        Console.WriteLine($"Arch packages:");
        foreach (var package in metadata.Packages)
            Console.WriteLine("- " + package);
        if (metadata.MissingOptionalDependencies != null)
        {
            if (metadata.MissingOptionalDependencies.Any())
            {
                Console.WriteLine($"Missing optional dependencies:");
                foreach (var package in metadata.MissingOptionalDependencies)
                    Console.WriteLine("- " + package);
            }
            else
            {
                Console.WriteLine($"Missing optional dependencies: none");
            }
        }
        
        // Get packages and provisions to check for conflicts (ensure packages that are installed are excluded)
        var packagesAndProvisionsToCheck = new List<string>();
        var packagesAlreadyInstalled = new List<IPackage>();
        foreach (var includedPackage in metadata.Packages)
        {
            var split = includedPackage.Split(' ');
            var name = split[0];
            var version = split[1];

            var installed = _systemPackages[name];
            if (installed != null)
            {
                packagesAlreadyInstalled.Add(installed);
                continue;
            }

            var providingPackages = _systemPackages.GetInstalledPackagesProvidingPackage(name);
            if (providingPackages.Any(x => x.ProvidedVersion == version))
                continue;

            packagesAndProvisionsToCheck.Add(includedPackage);
        }
        foreach (var provision in metadata.ProvidedPackages)
        {
            var split = provision.Split('=');
            var name = split[0];
            var version = split.Length > 1 ? split[1] : string.Empty;
            
            if (_systemPackages[name] != null)
                continue;
            
            var providingPackages = _systemPackages.GetInstalledPackagesProvidingPackage(name);
            if ((version == string.Empty && !providingPackages.IsEmpty) || providingPackages.Any(x => _versionComparer.IsVersionValid(version, x.ProvidedVersion)))
                continue;
            
            packagesAndProvisionsToCheck.Add($"{name} {version}");
        }
        
        // Get list of conflicts of the mango package, but exclude "false" conflicts
        var conflicts = metadata.Conflicts.ToHashSet();
        foreach (var alreadyInstalled in packagesAlreadyInstalled)
        foreach (var conflict in alreadyInstalled.ConflictingPackagesInfos)
            conflicts.Remove($"{conflict.Name}{conflict.VersionRequirement}");

        // Check for conflicts
        var conflictingPackages = _conflictDetector.GetInstalledConflictingPackages(packagesAndProvisionsToCheck, conflicts);
        if (conflictingPackages.Any())
        {
            Console.WriteLine($"Installed conflicts:");
            foreach (var conflict in conflictingPackages)
                Console.WriteLine("- " + conflict.Name);
        }
        else
        {
            Console.WriteLine("Installed conflicts: none");
        }

        return 0;
    }
}