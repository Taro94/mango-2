using CommandLine;

namespace Mango.Options;

[Verb("backup", HelpText = "Create a backup of an Arch package and its non-native dependencies.")]
public class BackupOptions {
        
    [Value(0, Default = "", HelpText = "Name of a package to create a backup of.", Required = true)]
    public string Package { get; set; }

    [Option('d', "directory", Required = false, HelpText = "Set the output directory.")]
    public string Directory { get; set; }

    [Option('f', "full", Required = false, HelpText = "Include installed optional dependencies.")]
    public bool WithOptionalDependencies { get; set; }
        
    [Option('r', "raw", Required = false, HelpText = "Create the backup in the form of loose Arch packages instead of a *.mango package that can be installed via mango.")]
    public bool RawPackages { get; set; }
}