namespace Mango.Interfaces;

/// <summary>
/// Type to check the distribution of the installed Linux.
/// </summary>
public interface IOsVerifier
{
    /// <summary>
    /// Method that checks if the running OS is ArchLinux-based.
    /// </summary>
    /// <returns>True if the OS is ArchLinux-based, false otherwise.</returns>
    bool GetIsOsArchLinux();
}