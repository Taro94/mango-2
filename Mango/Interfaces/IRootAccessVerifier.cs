namespace Mango.Interfaces;

/// <summary>
/// Type allowing to check if the application has root access.
/// </summary>
public interface IRootAccessVerifier
{
    /// <summary>
    /// Checks if the program has root privileges.
    /// </summary>
    /// <returns>True if root access is given, false otherwise.</returns>
    bool GetHasRootAccess();
}