using Mango.Options;

namespace Mango.Interfaces;

/// <summary>
/// Service to install mango packages.
/// </summary>
public interface IPackageInstaller
{
    /// <summary>
    /// Installs a mango package.
    /// </summary>
    /// <param name="options">Object containing configuration of the command.</param>
    /// <returns>0 if successful, 1 in case of errors.</returns>
    int InstallPackage(InstallOptions options);
}